const jwt = require("jsonwebtoken");
const User = require("../models").User;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const auth = async (req, res, next) => {
  try {    
    const token = req.header("Authorization").replace("Bearer ", "");
    
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const user = await User.findOne({ where: { code: payload.code,token:{[Op.ne]:null} }});

    if (!user) {
      throw new Error();
    }

    next();
  } catch (error) {
    res.status(401).send({ error: "Please login." });
  }
};
const createToken = (payload) => {
  return jwt.sign(payload, process.env.SECRET_KEY)
}

module.exports.auth = auth;
module.exports.createToken = createToken;
