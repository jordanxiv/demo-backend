const Sequelize = require("sequelize");

const sequelize = new Sequelize(process.env.DATABASE_NAME, process.env.DATABASE_USERNAME, process.env.DATABASE_PASSWORD, {
  host: process.env.DATABASE_HOST,
  dialect: "mysql",
  operatorAliases: false,
  define: { charset: "utf8", dialectOptions: { collate: "utf8_general_ci" }, timestamps: true }
});

module.exports = sequelize;
global.sequelize = sequelize;
