const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();

const app = express();

// DB connection
require("./config/connection");

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static("public"))

// Routes
const routes = require("./routes/index");
app.use("/api", routes);

// Server
const port = 3000;

app.listen(port, () => {
  console.log("Server in on port: " + port);
});
