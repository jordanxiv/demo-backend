"use strict";
module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define(
    "Course",
    {
      userId: DataTypes.INTEGER,
      title: DataTypes.STRING,
      subtitle: DataTypes.STRING,
      description: DataTypes.TEXT,
      categoryId: DataTypes.INTEGER,
      subcategoryId: DataTypes.INTEGER,
      levelId: DataTypes.INTEGER,
      priceId: DataTypes.INTEGER,
      thumbnail: DataTypes.STRING,
      isPublished: DataTypes.BOOLEAN,
      publishedAt: DataTypes.DATE
    },
    {}
  );
  Course.associate = function(models) {
    // associations can be defined here
    Course.hasMany(models.Video, { foreignKey: "courseId", as: "videos" });
    Course.belongsTo(models.User, { foreignKey: "userId", as: "owner" });
    Course.belongsToMany(models.User, {
      through: "course_reviews",
      as: "userReviews",
      foreignKey: "courseId"
    });
    Course.belongsToMany(models.Video, {
      through: "user_videos",
      as: "views",
      foreignKey: "courseId"
    });
  };
  return Course;
};
