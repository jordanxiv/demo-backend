"use strict";
module.exports = (sequelize, DataTypes) => {
  const user_video = sequelize.define(
    "user_video",
    {
      userId: DataTypes.INTEGER,
      courseId: DataTypes.INTEGER,
      videoId: DataTypes.INTEGER,
      duration: DataTypes.INTEGER,
      isWatched: DataTypes.BOOLEAN
    },
    {}
  );
  user_video.associate = function(models) {
    // associations can be defined here
    user_video.belongsTo(models.User, { foreignKey: "userId" });
    user_video.belongsTo(models.Course, { foreignKey: "courseId" });
    user_video.belongsTo(models.Video, { foreignKey: "videoId" });
  };
  return user_video;
};
