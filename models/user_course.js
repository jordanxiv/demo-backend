'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_course = sequelize.define('user_course', {
    userId: DataTypes.INTEGER,
    courseId: DataTypes.INTEGER,
    progress: DataTypes.DECIMAL 
  }, {});
  user_course.associate = function(models) {
    // associations can be defined here
    user_course.belongsTo(models.User, { foreignKey: "userId" });
    user_course.belongsTo(models.Course, { foreignKey: "courseId" });
  };
  return user_course;
};