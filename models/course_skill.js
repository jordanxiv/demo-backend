'use strict';
module.exports = (sequelize, DataTypes) => {
  const course_skill = sequelize.define('course_skill', {
    courseId: DataTypes.INTEGER,
    skillId: DataTypes.INTEGER
  }, {});
  course_skill.associate = function(models) {
    // associations can be defined here
  };
  return course_skill;
};