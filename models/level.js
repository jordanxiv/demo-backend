"use strict";
module.exports = (sequelize, DataTypes) => {
  const Level = sequelize.define(
    "Level",
    {
      name: DataTypes.STRING,
      nameTH: DataTypes.STRING,
      icon: DataTypes.STRING
    },
    {}
  );
  Level.associate = function(models) {
    // associations can be defined here
  };
  return Level;
};
