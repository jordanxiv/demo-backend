const Video = require("../models").Video;
const User = require("../models").User;
const UserVideo = require("../models").user_video;

module.exports = {
  // GET video views
  lastWatched: async (req, res) => {
    try {
      const videos = await UserVideo.findAll({
        where: {
          userId: req.query.userId,
          courseId: req.query.courseId
        },
        order: [["updatedAt", "desc"]]
      });

      res.send({
        status: true,
        data: videos
      });
    } catch (error) {
      res.status(500).send({
        status: false,
        error: error.message
      });
    }
  },
  // POST update user view video
  updateView: async (req, res) => {
    try {
      const video = await Video.findOne({
        where: {
          id: req.body.videoId
        }
      });

      if (!video) {
        return res.status(404).send("Video not found.");
      }

      const isWatched = req.body.isWatched;
      if (isWatched) {
        await video.update({
          watched: (video.watched += 1)
        });
      }

      await video.update({
        views: (video.views += 1)
      });

      const userVideos = await UserVideo.findOne({
        where: {
          userId: req.body.userId,
          videoId: req.body.videoId,
          courseId: req.body.courseId
        }
      });

      if (!userVideos) {
        await UserVideo.create({
          userId: req.body.userId,
          courseId: req.body.courseId,
          videoId: req.body.videoId,
          duration: req.body.duration
        });
      } else {
        await userVideos.update({
          userId: req.body.userId,
          courseId: req.body.courseId,
          videoId: req.body.videoId,
          duration: req.body.duration,
          isWatched: req.body.isWatched
        });
      }

      res.send({
        status: true,
        message: "Updated video views."
      });
    } catch (error) {
      res.status(500).send({
        status: false,
        error: error.message
      });
    }
  }
};
