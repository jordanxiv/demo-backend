const User = require("../models").User;
const Category = require("../models").Category;
const UserCategory = require("../models").user_category;
const Course = require("../models").Course;
const Review = require("../models").course_review;

module.exports = {
  //
  // USER
  //
  async getAuthUser(req, res) {
    try {
      const user = await User.findOne({
        where: {
          id: req.body.id
        }
      });

      res.send({
        status: true,
        data: user
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  },

  //
  // USER COURSES
  //
  async userEnrollCourse(req, res) {
    try {
      // Validate user
      const user = await User.findOne({
        where: {
          id: req.body.userId
        }
      });

      if (!user) {
        return res.status(400);
      }
      // Validate course
      const course = await Course.findOne({
        where: {
          id: req.body.courseId
        }
      });

      if (!course) {
        return res.status(404).send({
          message: "Course not found"
        });
      }
      // Create user_categories if user enroll to another categories
      const categoryId = course.categoryId;

      const userCategories = await UserCategory.findAll({
        where: {
          userId: req.body.userId,
          categoryId: categoryId
        }
      });

      if (userCategories.length == 0) {
        await UserCategory.create({
          userId: req.body.userId,
          categoryId: categoryId
        });
      }
      // Enroll the course
      const hasCourse = await UserCourse.findAll({
        where: {
          userId: req.body.userId,
          courseId: req.body.courseId
        }
      });

      if (hasCourse.length > 0) {
        return res.status(200).send({
          status: true,
          message: "You enrolled this course."
        });
      }

      const userCourse = await UserCourse.create({
        userId: req.body.userId,
        courseId: req.body.courseId
      });

      res.status(200).send({
        status: true,
        data: userCourse
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  },

  //
  // USER CATEGORIES
  //
  // GET user categories
  async getUserCategories(req, res) {
    try {
      const user = await User.findAll({
        where: {
          id: req.query.user
        },
        attributes: [],
        include: [
          {
            model: Category,
            as: "interests",
            attributes: ["name"],
            through: {
              attributes: []
            },
            include: [
              {
                model: Course,
                as: "courses",
                attributes: {
                  // include: [[Course.sequelize.literal(`(SELECT COUNT(*) FROM course_reviews WHERE course_reviews.courseId = Course.id)`), "requestsCount"]]
                },
                include: [
                  {
                    model: User,
                    as: "owner",
                    attributes: ["id", "name"]
                  },
                  {
                    model: User,
                    as: "userReviews",
                    // attributes: []
                  }
                ]
              }
            ]
          }
        ]
      });

      res.send({
        status: true,
        data: user
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  },
  // POST create user categories
  async createUserCategories(req, res) {
    try {
      const user = await User.findOne({
        where: {
          id: req.body.userId
        }
      });

      if (!user) {
        return res.status(400);
      }

      await user.update({
        hasInterest: req.body.hasInterest
      });

      const categories = req.body.categories;

      categories.forEach(async (item, index) => {
        const category = await Category.findOne({
          where: {
            id: categories[index]
          }
        });

        if (!category) {
          return res.status(400);
        }

        const userCategory = {
          userId: req.body.userId,
          categoryId: category.id
        };

        const saveUserCategory = await UserCategory.create(userCategory);
      });

      res.status(200).send({
        status: true,
        data: req.body
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  },

  // PUT Update user categories
  async updateUserCategories(req, res) {
    try {
      const user = await User.findOne({
        where: {
          id: req.body.userId
        }
      });

      if (!user) {
        return res.status(400);
      }

      const oldCategories = await user.getCategories();
      user.removeCategories(oldCategories);

      const categories = req.body.categories;

      categories.forEach(async (item, index) => {
        const category = await Category.findOne({
          where: {
            id: categories[index]
          }
        });

        if (!category) {
          return res.status(400);
        }

        const userCategory = {
          userId: req.body.userId,
          categoryId: category.id
        };

        const saveUserCategory = await UserCategory.create(userCategory);
      });

      res.status(200).send({
        status: true,
        data: req.body
      });
    } catch (error) {
      res.send({ error: error.message });
    }
  }
};
