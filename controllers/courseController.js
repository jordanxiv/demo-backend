const Course = require("../models").Course;
const Video = require("../models").Video;
const User = require("../models").User;
const CourseReview = require("../models").course_review;
const UserVideo = require("../models").user_video;

module.exports = {
  // GET /courses
  // GET /courses?limit=[Number]&skip=[Number]
  // GET /courses?category=[categoryId]&limit=[Number]
  getCourses: async (req, res) => {
    let query = {
      attributes: {
        include: [
          // Count videos
          [
            sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM videos AS video
                  WHERE
                      courseId = Course.id
              )`),
            "videoCount"
          ],
          // Count reviews
          [
            sequelize.literal(`(
                  SELECT COUNT(*)
                  FROM course_reviews AS review
                  WHERE
                      courseId = Course.id
              )`),
            "reviewCount"
          ],
          // Average rating
          [
            sequelize.literal(`(
                  SELECT CAST(AVG(rating) AS DECIMAL(10,2))
                  FROM course_reviews AS review
                  WHERE
                      courseId = Course.id
              )`),
            "avgRating"
          ]
        ]
      },
      include: [
        {
          model: User,
          as: "owner",
          attributes: ["id", "name", "content", "avatar"]
        },
      ],
      order: [
        [sequelize.literal("reviewCount"), "DESC"],
      ]
    };

    if (req.query.category) {
      query.where = {
        categoryId: req.query.category
      };
    }
    if (req.query.limit) {
      query.limit = parseInt(req.query.limit);
    }
    if (req.query.skip) {
      query.skip = parseInt(req.query.skip);
    }
    try {
      const courses = await Course.findAll(query);

      res.send({
        status: true,
        data: courses
      });
    } catch (error) {
      res.status(500).send({
        error: error.message
      });
    }
  },

  // get a single course by ID
  getCourseById: async (req, res) => {
    try {
      let query = {
        where: {
          id: req.params.id
        }
      };

      const course = await Course.findOne(query);

      if (!course) {
        return res.status(404).send({ message: "Course not found." });
      }

      res.send({
        status: true,
        data: course
      });
    } catch (error) {
      res.status(500).send({
        error: error.message
      });
    }
  }
};
