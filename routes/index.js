const router = require("express").Router();
const { auth } = require("../middlewares/auth");
const Subcategory = require("../models").Subcategory;

const { register, login, logout } = require("../controllers/authController");
const { getUserCategories, createUserCategories, getAuthUser, updateUserCategories, userEnrollCourse } = require("../controllers/userController");
const { getCategories } = require("../controllers/categoryController");
const { getCourseById, getCourses } = require("../controllers/courseController");
const { createCourseReview } = require("../controllers/reviewController");
const { lastWatched, updateView } = require("../controllers/videoController");

// AUTH
router.post("/register", register);
router.post("/login", login);
router.post("/logout", logout);

// USER
router.post("/user/enroll/course", userEnrollCourse);

// USER_CATEGORIES
router.post("/me", getAuthUser);
router.get("/discovery", getUserCategories);
router.post("/create/user/categories", createUserCategories);
router.put("/update/user/categories", updateUserCategories);

// CATEGORY
router.get("/categories", getCategories);

// SUBCATEGORY
router.get("/subcategories", async (req, res) => {
  try {
    const subcategories = await Subcategory.findAll();

    res.send({
      status: true,
      data: subcategories
    });
  } catch (error) {}
});

// COURSE
router.get("/course/:id", getCourseById);
// GET /courses
// GET /courses?limit=[Number]&skip=[Number]
// GET /courses?category=[categoryId]&limit=[Number]
router.get("/courses", getCourses);

// COURSE_REVIEW
// POST create review
router.post("/review/:courseId", createCourseReview);

// VIDEO
router.get("/lastwatched", lastWatched);
router.post("/video/views", updateView);

module.exports = router;
