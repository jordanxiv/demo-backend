"use strict";
const faker = require("faker");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const categories = [
      "development",
      "business",
      "finance & account",
      "it & software",
      "office productivity",
      "personal development",
      "design",
      "marketing",
      "lifestyle",
      "photography",
      "health & fitness",
      "music",
      "academics & teaching",
      "mother & child",
      "languages",
      "examination",
      "cooking",
      "agriculture & farming"
    ];
    const categoriesTH = [
      "เทคสตาร์ตอัพ",
      "ธุรกิจ",
      "การเงิน & การลงทุน",
      "คอมพิวเตอร์",
      "การทำงาน & อาชีพ",
      "พัฒนาตัวเอง",
      "กราฟฟิก & ออกแบบ",
      "การตลาด",
      "ไลฟ์สไตล์",
      "ถ่ายภาพ",
      "สุขภาพ & การออกกำลังกาย",
      "ดนตรี",
      "โรงเรียน & มหาวิทยาลัย",
      "แม่และเด็ก ",
      "ภาษา",
      "เตรียมสอบ",
      "ทำอาหาร",
      "การเกษตร & ฟาร์ม"
    ];
    const icons = [
      "fab fa-react",
      "fal fa-user-tie",
      "fal fa-chart-line",
      "fal fa-laptop-code",
      "fal fa-bullseye-arrow",
      "fal fa-head-side",
      "fal fa-drafting-compass",
      "fal fa-store",
      "fal fa-smile-wink",
      "fal fa-camera-retro",
      "fal fa-heartbeat",
      "fal fa-guitar",
      "fal fa-graduation-cap",
      "fal fa-baby",
      "fal fa-language",
      "fal fa-books",
      "fal fa-utensils",
      "fal fa-tree-alt"
    ];
    let categoriesData = [];
    for (let i = 0; i < categories.length; i++) {
      categoriesData.push({
        id: i + 1,
        name: categories[i],
        nameTH: categoriesTH[i],
        icon: icons[i],
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }
    return queryInterface.bulkInsert("categories", categoriesData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("categories", null, {});
  }
};
