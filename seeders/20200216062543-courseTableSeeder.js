"use strict";

const faker = require("faker");
const Subcategory = require("../models").Subcategory;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let courses = [];
    let amount = 500;

    function randomBetween(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    const subcategories = await Subcategory.findAll();

    async function randomSubcategory(categoryId) {
      let subcategoriesId = await subcategories.filter(v => v.categoryId == categoryId);
      let minId = subcategoriesId[0].id;
      let maxId = subcategoriesId[subcategoriesId.length - 1].id;

      return await randomBetween(minId, maxId);
    }

    // image collections from Unsplash.com
    let imagesCollection = [1163637, 190727, 6780963, 1198107, 762960, 1353633, 3321491, 217461, 2203755];
    let imageWidth = 980;
    let imageHeight = 555;

    var uniqueNumbers = [];
    while (uniqueNumbers.length < amount) {
      var r = Math.floor(Math.random() * 1000) + 1;
      if (uniqueNumbers.indexOf(r) === -1) uniqueNumbers.push(r);
    }

    while (amount--) {
      let randomCategoryId = await randomBetween(1, 18);
      let subcategoryId = await randomSubcategory(randomCategoryId);
      let randomCollection = await randomBetween(0, imagesCollection.length - 1);
      let randomThumbnail = `https://source.unsplash.com/collection/${imagesCollection[randomCollection]}/${imageWidth}x${imageHeight}/?sig=${uniqueNumbers[amount]}`;

      courses.push({
        userId: randomBetween(1, 50),
        title: faker.lorem.sentence(),
        subtitle: faker.lorem.sentence(),
        description: faker.lorem.paragraphs(),
        categoryId: randomCategoryId,
        subcategoryId: subcategoryId,
        levelId: randomBetween(1, 4),
        priceId: randomBetween(1, 2),
        thumbnail: randomThumbnail,
        isPublished: true,
        publishedAt: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }

    return await queryInterface.bulkInsert("courses", courses, {});
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert("courses", null, {});
  }
};
