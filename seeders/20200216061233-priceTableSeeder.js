"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    const items = ["Premium", "Free"];
    const itemsTH = ["พรีเมี่ยม", "ฟรี"];
    let itemData = [];
    for (let i = 0; i < items.length; i++) {
      itemData.push({
        id: i + 1,
        name: items[i],
        nameTH: itemsTH[i],
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }
    return queryInterface.bulkInsert("prices", itemData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("prices", null, {});
  }
};
