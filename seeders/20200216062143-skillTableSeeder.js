"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let skills = [];
    let amount = 200;

    while (amount--) {
      skills.push({
        name: faker.lorem.word(),
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }
    return await queryInterface.bulkInsert("skills", skills, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("skills", null, {});
  }
};
