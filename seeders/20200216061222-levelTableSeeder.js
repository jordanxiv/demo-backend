"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    const items = ["Beginner", "Intermediate", "Expert", "All Levels"];
    const itemsTH = ["เริ่มต้น", "ปานกลาง", "ผู้เชี่ยวชาญ", "ทุกระดับ"];
    let itemData = [];
    for (let i = 0; i < items.length; i++) {
      itemData.push({
        id: i + 1,
        name: items[i],
        nameTH: itemsTH[i],
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }
    return queryInterface.bulkInsert("levels", itemData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("levels", null, {});
  }
};
