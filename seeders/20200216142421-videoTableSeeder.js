"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let videos = [];
    let courses = 500;

    function randomBetween(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    let exampleVideos = [
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4"
    ];

    for (let i = 0; i < courses; i++) {
      let videosAmount = await randomBetween(3, 12);

      for (let n = 0; n < videosAmount; n++) {
        let randomVideoIndex = await randomBetween(0, exampleVideos.length - 1);
        let exampleVideo = await exampleVideos[randomVideoIndex];

        videos.push({
          courseId: i + 1,
          title: faker.lorem.sentence(),
          order: n + 1,
          duration: randomBetween(250, 1800),
          thumbnail: "https://cdn.vuetifyjs.com/images/cards/forest-art.jpg",
          views: 0,
          watched: 0,
          quality_360: exampleVideo,
          quality_480: exampleVideo,
          quality_720: exampleVideo,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    }

    return await queryInterface.bulkInsert("videos", videos, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("videos", null, {});
  }
};
