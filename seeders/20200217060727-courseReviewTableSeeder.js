"use strict";

const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let reviews = [];
    let users = 50;
    let courses = 50;

    function randomBetween(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    for (let i = 0; i < courses; i++) {
      let randomReviews = randomBetween(1, 50);

      var uniqueUser = [];
      while (uniqueUser.length < randomReviews) {
        var r = Math.floor(Math.random() * users) + 1;
        if (uniqueUser.indexOf(r) === -1) uniqueUser.push(r);
      }

      for (let n = 0; n < uniqueUser.length; n++) {
        reviews.push({
          title: faker.lorem.sentence(),
          content: faker.lorem.sentence(),
          rating: randomBetween(2, 5),
          courseId: i + 1,
          userId: uniqueUser[n],
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    }

    return await queryInterface.bulkInsert("course_reviews", reviews, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("course_reviews", null, {});
  }
};
